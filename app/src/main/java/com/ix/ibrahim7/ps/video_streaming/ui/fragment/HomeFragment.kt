package com.ix.ibrahim7.ps.video_streaming.ui.fragment

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.ix.ibrahim7.ps.video_streaming.databinding.FragmentHomeBinding
import com.ix.ibrahim7.ps.video_streaming.ui.other.*

class HomeFragment : Fragment() {

    private val mbinding by lazy {
        FragmentHomeBinding.inflate(layoutInflater)
    }

    private var simpleExoplayer: SimpleExoPlayer? = null
    private var playbackPosition: Long = 0

    private val dataSourceFactory: DataSource.Factory by lazy {
        DefaultDataSourceFactory(requireContext(), USER_AGENT)
    }

    private var videoUrl: String? = null
    private var videoType: String? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = mbinding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mbinding.apply {
            btnVideo1.setOnClickListener {
                videoUrl = URL1
                videoType = DEFAULT
                initializePlayer(URL1, DEFAULT)
            }
            btnVideo2.setOnClickListener {
                videoUrl = URL4
                videoType = DASH
                initializePlayer(URL4, DASH)
            }
            btnVideo3.setOnClickListener {
                videoUrl = URL2
                videoType = DEFAULT
                initializePlayer(URL2, DEFAULT)
            }
            btnVideo4.setOnClickListener {
                videoUrl = URL3
                videoType = DEFAULT
                initializePlayer(URL4, DEFAULT)
            }
        }

    }


    private fun initializePlayer(uri: String, type: String) {
        simpleExoplayer = SimpleExoPlayer.Builder(requireContext()).build()
        preparePlayer(uri, type)
        mbinding.videoPlayer.player = simpleExoplayer
        simpleExoplayer?.seekTo(playbackPosition)
        simpleExoplayer?.playWhenReady = true
    }

    private fun buildMediaSource(uri: Uri, type: String): MediaSource {
        return if (type == DASH) {
            DashMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)
        } else {
            ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)
        }
    }

    private fun preparePlayer(videoUrl: String, type: String) {
        val uri = Uri.parse(videoUrl)
        val mediaSource = buildMediaSource(uri, type)
        simpleExoplayer?.prepare(mediaSource)
    }

    private fun releasePlayer() {
        playbackPosition = simpleExoplayer?.currentPosition ?: 0
        simpleExoplayer?.release()
    }

    override fun onResume() {
        super.onResume()
        if (videoUrl != null)
            initializePlayer(videoUrl!!, videoType!!)
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

}